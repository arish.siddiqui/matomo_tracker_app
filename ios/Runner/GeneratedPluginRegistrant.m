//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<flutter_piwikpro/FlutterPiwikProPlugin.h>)
#import <flutter_piwikpro/FlutterPiwikProPlugin.h>
#else
@import flutter_piwikpro;
#endif

#if __has_include(<sms_autofill/SmsAutoFillPlugin.h>)
#import <sms_autofill/SmsAutoFillPlugin.h>
#else
@import sms_autofill;
#endif

#if __has_include(<url_launcher_ios/FLTURLLauncherPlugin.h>)
#import <url_launcher_ios/FLTURLLauncherPlugin.h>
#else
@import url_launcher_ios;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [FlutterPiwikProPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterPiwikProPlugin"]];
  [SmsAutoFillPlugin registerWithRegistrar:[registry registrarForPlugin:@"SmsAutoFillPlugin"]];
  [FLTURLLauncherPlugin registerWithRegistrar:[registry registrarForPlugin:@"FLTURLLauncherPlugin"]];
}

@end
