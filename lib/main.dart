import 'package:flutter/material.dart';
import 'package:flutter_piwikpro/flutter_piwikpro.dart';
import 'package:tracker_app/sms_auto_fill.dart';
// import 'package:matomo/matomo.dart';
// import 'package:logging/logging.dart';
// import 'package:matomo_tracker/matomo_tracker.dart';
import 'package:url_launcher/url_launcher.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await MatomoTracker.instance.initialize(
  //   siteId: 42,
  //   url: 'https://analyticspoc.firsthive.com/matomo.php',
  //   verbosityLevel: Level.all,
  //   // dispatchSettings: dispatchSettingsEndToEndTest,
  // );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key}) {
    // Logger.root.level = Level.FINEST;
    // Logger.root.onRecord.listen((LogRecord rec) {
    //   print(
    //       '[${rec.time}][${rec.level.name}][${rec.loggerName}] ${rec.message}');
    // });

    // MatomoTracker().initialize(
    //   siteId: 42,
    //   url: 'https://analyticspoc.firsthive.com/matomo.php',
    // );
  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Matomo Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const SmsAutoFillPage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    // MatomoTracker.instance.trackEvent(
    //   eventInfo: EventInfo(
    //     category: 'Main',
    //     action: 'Click',
    //     name: 'IncrementCounter',
    //   ),
    //   // 'IncrementCounter',
    //   // 'Click',
    //   // eventValue: 2,
    //   // widgetName: 'Increment Button',
    // );
    setState(() {
      _counter++;
    });
  }

  late FlutterPiwikPro flutterPiwik;

  @override
  void initState() {
    // MatomoTracker.instance.trackEvent(
    //   eventInfo: EventInfo(
    //     category: category,
    //     action: action,
    //   ),
    // );
    // MatomoTracker.trackEvent('PageLoad', 'Page Loaded',
    //     eventValue: 1, widgetName: 'MyHomePage');
    // MatomoTracker.
    setUpInit();
    debugPrint('PageLoaded');
    super.initState();
  }

  setUpInit() async {
    flutterPiwik = FlutterPiwikPro.sharedInstance;
    final res = await flutterPiwik.configureTracker(
      baseURL: 'https://analyticspoc.firsthive.com/matomo.php',
      siteId: '42',
    );
    debugPrint('Res : $res');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Motamo Tracker'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () {},
              child: const Text('Button 1'),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () {
                'https://www.africau.edu/images/default/sample.pdf';
              },
              child: const Text('Button 2'),
            ),
            const SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () async {
                // MatomoTracker.instance.trackOutlink(
                //   link: 'https://www.africau.edu/images/default/sample.pdf',

                // );
                final Uri url = Uri.parse(
                    'https://www.africau.edu/images/default/sample.pdf');
                if (await canLaunchUrl(url)) {
                  await launchUrl(
                    url,
                    // mode: LaunchMode.externalApplication,
                  );
                } else {
                  // throw 'Could not launch url';
                  print('Could not launch url');
                }
                // final String fileName =
                //     'https://www.africau.edu/images/default/sample.pdf'
                //         .split('/')
                //         .last;
                final result = await flutterPiwik.trackDownload(
                  'https://www.africau.edu/images/default/sample.pdf',
                );
                debugPrint('trackDownload: $result');
                // MatomoTracker.instance.trackEvent(
                //   eventInfo: EventInfo(
                //     category: 'File Downloads',
                //     action: 'Download',
                //     name:
                //         fileName, // Replace with the name of the downloaded file
                //     // value: 1,
                //   ), // You can specify a value for the event if needed
                // );
              },
              child: const Text('Download'),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}

// class MyHomePage extends TraceableStatefulWidget {
//   const MyHomePage({Key? key}) : super(key: key, name: 'MyHomePage');

//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   int _counter = 0;

//   void _incrementCounter() {
//     MatomoTracker.trackEvent('IncrementCounter', 'Click',
//         eventValue: 2, widgetName: 'Increment Button');
//     setState(() {
//       _counter++;
//     });
//   }

//   @override
//   void initState() {
//     MatomoTracker.trackEvent(
//       'PageLoad',
//       'Page Loaded',
//       eventValue: 1,
//       widgetName: 'MyHomePage',
//     );
//     // MatomoTracker.
//     debugPrint('PageLoaded');
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Motamo'),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             const Text(
//               'You have pushed the button this many times:',
//             ),
//             Text(
//               '$_counter',
//               style: Theme.of(context).textTheme.headlineMedium,
//             ),
//             const SizedBox(height: 10.0),
//             ElevatedButton(
//               onPressed: () {},
//               child: const Text('Button 1'),
//             ),
//             const SizedBox(height: 10.0),
//             ElevatedButton(
//               onPressed: () {
//                 'https://www.africau.edu/images/default/sample.pdf';
//               },
//               child: const Text('Button 2'),
//             ),
//             const SizedBox(height: 10.0),
//             ElevatedButton(
//               onPressed: () {},
//               child: const Text('Button 3'),
//             ),
//           ],
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: _incrementCounter,
//         tooltip: 'Increment',
//         child: const Icon(Icons.add),
//       ),
//     );
//   }
// }
