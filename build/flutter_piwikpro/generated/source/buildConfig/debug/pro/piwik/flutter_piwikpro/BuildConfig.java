/**
 * Automatically generated file. DO NOT MODIFY
 */
package pro.piwik.flutter_piwikpro;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "pro.piwik.flutter_piwikpro";
  public static final String BUILD_TYPE = "debug";
}
